#!/bin/bash
# Monitoring script for MySQL replication heartbeat
#
# Author: Sigbjorn Dybdahl
# $Id: Check_db_repl_heartbeat 14595 2012-08-08 09:52:47Z sigbjorn

# Checking if it's the active master
active=`mysql -udb_user -p2store4GOOD -e "SHOW VARIABLES LIKE 'read_only'" | grep read_only | awk '{ print $2}'`
if [ ${active} == "OFF" ]; then
        # read_only = OFF, which means it's the active unit

        # Finding the master-server-id
        master=`ip addr show | grep inet | grep -v inet6 | grep -v 127.0.0.1 | head -1 | awk '{print $2}' | awk -F/ '{print $1}' | awk -F. '{print $4}'`

        # Finding passive master ip
        passive=`mysql -udb_user -p2store4GOOD -e "show slave status\G" | grep Master_Host | awk '{print $2}'`

        # Finding replicated databases
        databases=( `grep 'replicate_do_db' /etc/mysql/my.cnf | awk '{ print $3}'` )
        return_code=0
        for database in "${databases[@]}"; do
                # 1- checking that the heartbeat daemon is present
                daemons=`ps -ef | grep pt-heartbeat | grep daemonize | grep ${database^^} | wc -l`
                if [ ${daemons} -eq 1 ]; then
                        # 2- launching the heartbeat check script
                        result=`pt-heartbeat -u db_user -p 2store4GOOD -D "${database^^}" --table=PT_HEARTBEAT --master-server-id ${master} --check ${passive} | awk -F. '{print $1}'`

                        # 3- parsing result and determining the value of the return code
                        if [ ${result} -lt 10 ]; then
                                # Doing nothing
                                if [ ${return_code} -lt 0 ]; then
                                        return_code=0
                                        #exit 0
                                fi
                        elif [ ${result} -lt 30 ]; then
                                echo "${database^^}: Replication delay for: ${result}"
                                if [ ${return_code} -lt 1 ]; then
                                        return_code=1
                                        #exit 1
                                fi
                        else
                                echo "${database^^}: Replication delay: ${result}"
                                if [ ${return_code} -lt 2 ]; then
                                        return_code=2
                                        #exit 2
                                fi
                        fi
                else
                        echo "${database^^}: Heartbeat daemon absent"
                        if [ ${return_code} -lt 3 ]; then
                                return_code=3
                                #exit 3
                        fi
                fi
        done
        echo "Plugin returned \${return_code} = ${return_code}"
        exit ${return_code}
else
        # read_only = ON, which means it's the passive unit
        echo "CHECK OK !"
        exit 0
fi