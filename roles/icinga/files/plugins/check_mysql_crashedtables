#!/bin/sh
#########################################################################
# Script:	check_mysql_crashedtables.sh				#
# Author:	Laurent Edel laurent.edel@gmail.com			#
# Purpose:	Monitor MySQL crashed tables with Nagios		#
# Description:	Connects to given MySQL hosts and checks for crashed 	#
#		tables (TABLE_TYPE is not VIEW and ENGINE NULL
#########################################################################
# Usage: ./check_mysql_crashedtables.sh -H dbhost -P port -u dbuser -p dbpass
#########################################################################
help="\ncheck_mysql_slavestatus.sh (c) 2008 GNU licence
Usage: ./check_mysql_slavestatus.sh -H host P port -u username -p password\n
Options:\n-H Hostname\n-P Port of MySQL Server (3306 is standard)\n-u Username of DB-User\n-p Password of DBUser\n"

STATE_OK=0		# define the exit code if status is OK
STATE_WARNING=1		# define the exit code if status is Warning (not really used)
STATE_CRITICAL=2	# define the exit code if status is Critical
STATE_UNKNOWN=3		# define the exit code if status is Unknown
PATH=/usr/local/bin:/usr/bin:/bin # Set path
port=3306
host=$1
user=$2
password=$3

for cmd in mysql cut grep [ 
do
 if ! `which ${cmd} 1>/dev/null`
 then
  echo "UNKNOWN: ${cmd} does not exist, please check if command exists and PATH is correct"
  exit ${STATE_UNKNOWN}
 fi
done

# Connect to the DB server and check for informations
#########################################################################
if ! `mysql -h ${host} -P ${port} -u ${user} --password=${password} -e 'show slave status\G' 1>/dev/null 2>/dev/null`
then
 echo CRITICAL: unable to connect to server
 exit ${STATE_CRITICAL}
fi

check1=$(mysql -h ${host} -P ${port} -u ${user} --password=${password}  --batch --skip-column-names -e 'SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE "DB\_%" AND TABLE_TYPE != "VIEW" AND ENGINE IS NULL' | awk '{ print $2 "." $3}')

if [ ! -z ${check1} ]; then
echo CRITICAL: Table ${check1} is marked as crashed !
exit ${STATE_CRITICAL};
fi

echo OK : no table found crashed \o/
exit ${STATE_OK}
