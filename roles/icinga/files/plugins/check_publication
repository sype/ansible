#! /bin/bash
#############################################################################################################################################
# Author: SNOUSSI Ramzi                                                                                                                     #
# Email : ramzi@fifty-five.com                                                                                                              #
# This plugin looks for mysql instances publication tasks older than 24 hours presenting a non empty status (Complete/Error/Other)          #
# Empty status entries : check OK with "Every thing seems OK !" as a return message                                                         #
# Status "Complete"    : check OK with Complete tasks as a return message                                                                   #
# Status "Error"       : Critical alert with Error tasks as a retrun message                                                                #
# Other status         : Warning alert with task presenting these status as a return message                                                #
#############################################################################################################################################


## display usage

if [ "$#" -lt "2" ]; then
	echo "Usage :   <plugin name>       instance_name   publication_name"
	echo "Example: ./check_publication  3suisses        TSO_3SUISSES_MOVER"
	exit 2
fi

STATE_OK=0
STATE_WARNING=1;
STATE_CRITICAL=2;
STATUS=""
INSTANCE_NAME=$(echo "$1" | awk '{print tolower($0)}')
PUBLICATION_NAME=$(echo "$2" | awk '{print toupper($0)}')
BASE_NAME="DB_$(echo "$1" | awk '{print toupper($0)}')"
HOSTNAME="$3"
SERVICE_DESCRIPTION="$4"

## temporary files
COMMAND_RESULT_TEMP_FILE="/tmp/$INSTANCE_NAME.$PUBLICATION_NAME.COMMAND_RESULT"
ERRORS_TEMP_FILE="/tmp/$INSTANCE_NAME.$PUBLICATION_NAME.ERROR_STATUS"
COMPLETES_TEMP_FILE="/tmp/$INSTANCE_NAME.$PUBLICATION_NAME.COMPLETE_STATUS"
OTHERS_TEMP_FILE="/tmp/$INSTANCE_NAME.$PUBLICATION_NAME.OTHER_STATUS"
echo "" > $COMMAND_RESULT_TEMP_FILE
echo "" > $COMPLETES_TEMP_FILE 
echo "" > $ERRORS_TEMP_FILE
echo "" > $OTHERS_TEMP_FILE
chmod 777 $COMMAND_RESULT_TEMP_FILE
chmod 777 $COMPLETES_TEMP_FILE 
chmod 777 $ERRORS_TEMP_FILE
chmod 777 $OTHERS_TEMP_FILE


function clean_logs(){
rm $COMMAND_RESULT_TEMP_FILE $COMPLETES_TEMP_FILE $ERRORS_TEMP_FILE $OTHERS_TEMP_FILE
}

## Mysql Request 
REQUEST='SELECT t.TASK_ID, NAME, CATEGORY, t.STATUS, DT_UPDATED, DATE_SUB(NOW(), INTERVAL 24 HOUR) 
FROM TASKS t
LEFT JOIN  `TASK_DETAILS` d ON t.TASK_ID = d.TASK_ID
LEFT JOIN  `PUBLICATION_DESTINATION` p ON d.ENTITY_ID = p.PUBLICATION_DESTINATION_ID
WHERE p.CATEGORY =  '\"$PUBLICATION_NAME\"'
AND ((t.STATUS NOT IN ("Complete",  "Error") AND t.DT_UPDATED > DATE_SUB(NOW() , INTERVAL 24 HOUR)) || t.STATUS =  "Error")'



## run mysql request and save result
mysql -h "$INSTANCE_NAME.55labs.com" -D "$BASE_NAME" -u nagios -pnagiosPowa --execute="$REQUEST" > $COMMAND_RESULT_TEMP_FILE

## scroll request result temp file looking for status
## every non empty status is saved in the correspondant temp file
if [ $? = 0 ]; then
	cat $COMMAND_RESULT_TEMP_FILE | while read LINE; do
		STATUS=$(echo "$LINE" | sed 's/\s\s*/ /g' | cut -d ' ' -f 4)
		if [ $STATUS = "Error" ]; then
			echo "$LINE" >> $ERRORS_TEMP_FILE
		elif [ $STATUS = "Complete" ]; then
			echo "$LINE" >> $COMPLETES_TEMP_FILE
		elif [ $STATUS = "STATUS" ]; then 
			echo "TASK_ID NAME     CATEGORY        STATUS  DT_UPDATED      DATE_SUB(NOW(), INTERVAL 24 HOUR)" > $ERRORS_TEMP_FILE
			echo "TASK_ID NAME      CATEGORY        STATUS  DT_UPDATED      DATE_SUB(NOW(), INTERVAL 24 HOUR)" > $COMPLETES_TEMP_FILE
			echo "TASK_ID NAME      CATEGORY        STATUS  DT_UPDATED      DATE_SUB(NOW(), INTERVAL 24 HOUR)" > $OTHERS_TEMP_FILE
		else
			echo "$LINE" >> $OTHERS_TEMP_FILE
		fi
	done
else
	exit 2
fi

## count files lines number
ERRORS=$(cat $ERRORS_TEMP_FILE | wc -l)
COMPLETES=$(cat $COMPLETES_TEMP_FILE | wc -l)
OTHERS=$(cat $OTHERS_TEMP_FILE | wc -l)


## return messages depend of temp files lines number
## return messages are entries that show non empty status
if [ $ERRORS -gt 1 ]; then
	cat $ERRORS_TEMP_FILE | while read LIGNE; do
		echo "$LIGNE"
	done
	clean_logs
	exit $STATE_CRITICAL
elif [ $OTHERS -gt 1 ]; then
	cat $OTHERS_TEMP_FILE | while read LIGNE; do
		echo "$LIGNE"
	done
	clean_logs
	exit $STATE_WARNING
elif [ $COMPLETES -gt 1 ]; then
	cat $COMPLETES_TEMP_FILE | while read LIGNE; do
		echo "$LIGNE"
	done
	clean_logs
	exit $STATE_OK
else
	echo "Every thing seems OK !"
	clean_logs
	exit $STATE_OK
fi
