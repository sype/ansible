#!/bin/bash
# Monitoring script for MySQL replication checksum
#
# Author: Sigbjorn Dybdahl
# $Id: check_db_repl_checksum 14595 2012-08-08 09:52:47Z sigbjorn

# Checking if it's the active master
active=`mysql -udb_user -p2store4GOOD -e "SHOW VARIABLES LIKE 'read_only'" | grep read_only | awk '{ print $2 }'`
if [ ${active} == "OFF" ]; then
        # read_only = OFF, which means it's the active unit
        # Checking sentinel file presence (file newer than 90 minutes)
        sentinel=`find /var/tmp/ -iname pt-table-checksum -mmin -90 | wc -l`
        if [ ${sentinel} -eq 0 ]; then
                # Sentinel file not present or too old, relaunching the checksum
                if [ -f /var/tmp/pt-table-checksum ]; then
                        rm -f /var/tmp/pt-table-checksum
                fi

                # Finding replicated databases
        databases=( `grep 'replicate_do_db' /etc/mysql/my.cnf | awk '{ print $3 }'` )
        for database in "${databases[@]}"; do
                        # Checking that database checksum isn't already launched
                        process=`ps -ef | grep pt-table-checksum | grep ${database} | wc -l`

                        if [ ${process} -eq 0 ]; then
                                # Launching the database checksum
                                result=`pt-table-checksum -h localhost -u db_user -p 2store4GOOD --create-replicate-table --replicate ${database}.PT_CHECKSUM --databases ${database} --nocheck-replication-filters --quiet --quiet > /var/tmp/pt-table-checksum-${database} &`
                        fi
                done

                # Creating a new sentinel file
                echo "99" > /var/tmp/pt-table-checksum

                # Return 0 for now since we don't know the result yet
                echo "CHECK OK !"
                exit 0
        else
                # Sentinel file present, checking content
                result=`awk '{ print $1 }' /var/tmp/pt-table-checksum`

                if [ ${result} -eq 99 ]; then
                        # Checking that database checksum isn't already launched
                        process=`ps -ef | grep pt-table-checksum | wc -l`

                        if [ ${process} -eq 0 ]; then
                                return_code=0
                                # Finding replicated databases
                                databases=( `grep 'replicate_do_db' /etc/mysql/my.cnf | awk '{ print $3}'` )
                for database in "${databases[@]}"; do
                                        # Checking database checksum result
                                        temp=`wc -l "/var/tmp/pt-table-checksum-${database}" | awk '{print $1}'`

                                        if [ ${temp} -gt 0 ]; then
                                                echo "${database}: Checksum failed (details in /var/tmp/pt-table-checksum-${database})"
                                                if [ ${return_code} -lt 2 ]; then
                                                        return_code=2
                                                fi
                                        fi
                                done

                                # Writing return code to sentinel
                                echo ${return_code} > /var/tmp/pt-table-checksum

                                result=${return_code}
                        else
                                # Not finished launching database checksums
                                result=0
                        fi
                fi

                # Return result
                echo "Plugin returned \${result} = ${result}"
                exit ${result}
        fi
else
        # read_only = ON, which means it's the passive unit
        echo "CHECK OK !"
        exit 0
fi