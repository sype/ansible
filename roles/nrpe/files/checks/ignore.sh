#! /bin/bash
#############################################################################################################################################
# Author: SNOUSSI Ramzi                                                                                                                     #
# Email : ramzi@fifty-five.com                                                                                                              #
#############################################################################################################################################


OUTPUT=$1
HOSTNAME=$2
SERVICE_DESCRIPTION=$3
COMMENTS_FILE="/tmp/nagios_comments.temp"

## démarrer l'api nagios-api
# sudo ./nagios-api -p 8080 -c /usr/local/nagios/var/rw/nagios.cmd -l /usr/local/nagios/var/nagios.log -s /usr/local/nagios/var/status.dat&
## récupérer les commentaires selon le couple hostname/servicedesc
chmod 777 $COMMENTS_FILE
curl http://dmp05.55labs.com:8080/state | jq '.content.'"$HOSTNAME"'.services.'"$SERVICE_DESCRIPTION"'.comments | to_entries[].value.comment_data'  | sed 's/\\"//g' | sed 's/"//g'> $COMMENTS_FILE
if [ `cat "$COMMENTS_FILE" | wc -l` -gt 0 ]; then
OUTPUT_TYPE=$(grep "output_type" "$COMMENTS_FILE" | cut -d ":" -f2)

case "$OUTPUT_TYPE" in
        "json")
        while read OBJECT_TO_IGNORE; do
                [ "$OBJECT_TO_IGNORE" != "" ] && OBJECT_TO_IGNORE=$(echo "$OBJECT_TO_IGNORE" | sed 's/ *: */:/g' | sed 's/"//g')
                [ "$OBJECT_TO_IGNORE" != "" ] && OBJECT_TO_IGNORE_KEY=$(echo "$OBJECT_TO_IGNORE" | cut -d ':' -f1)
                [ "$OBJECT_TO_IGNORE" != "" ] && OBJECT_TO_IGNORE_VALUE=$(echo "$OBJECT_TO_IGNORE" | cut -d ':' -f2-)
                [ "$OBJECT_TO_IGNORE" != "" ] && OUTPUT=$(echo $OUTPUT | jq 'map(select(.'"$OBJECT_TO_IGNORE_KEY"' != '\""$OBJECT_TO_IGNORE_VALUE"\"'))')
        done < /tmp/nagios_comments.temp
                rm /tmp/nagios_comments.temp
                echo "$OUTPUT" 
                exit 0
                ;;
        "text")
                rm /tmp/nagios_comments.temp
                exit 1
                ;;
        *)
                rm /tmp/nagios_comments.temp
                exit 2
                ;;
esac
else
  echo "$OUTPUT"
  exit 0
fi
